use std::mem;

pub fn merge<Ax: IntoIterator<Item=E>, Bx: IntoIterator<Item=E>, E, F: Fn(&E, &E) -> bool>(ax: Ax, bx: Bx, f: F) -> Vec<E> {
    let (mut ax, mut bx) = (ax.into_iter(), bx.into_iter());
    let mut acc = vec![];
    let (mut a, mut b) = (ax.next(), bx.next());
    while a.is_some() && b.is_some() {
        if f(a.as_ref().unwrap(), b.as_ref().unwrap()) {
            acc.push(mem::replace(&mut a, ax.next()).unwrap());
        } else {
            acc.push(mem::replace(&mut b, bx.next()).unwrap());
        }
    }
    while a.is_some() {
        acc.push(mem::replace(&mut a, ax.next()).unwrap());
    }
    while b.is_some() {
        acc.push(mem::replace(&mut b, bx.next()).unwrap());
    }
    acc
}

pub fn do_sort<E>(a: &mut Vec<E>, f: &Fn(&E, &E) -> bool) {
    if a.len() < 2 {
        return;
    }
    let mp = a.len() / 2;
    let mut b = a.split_off(mp);
    do_sort(a, f);
    do_sort(&mut b, f);
    let r = merge(a.drain(..), b.drain(..), |x, y| f(x, y));
    *a = r;
}

pub fn sort_by<E, F: Fn(&E, &E) -> bool>(a: &mut Vec<E>, f: F) {
    do_sort(a, &f);
}

pub fn sort<E: PartialOrd>(a: &mut Vec<E>) {
    sort_by(a, |x, y| x < y);
}
